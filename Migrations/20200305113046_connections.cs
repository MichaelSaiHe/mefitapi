﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFit_API.Migrations
{
    public partial class connections : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Profiles_AddressId",
                table: "Profiles",
                column: "AddressId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_GoalId",
                table: "Profiles",
                column: "GoalId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_ProgramsId",
                table: "Profiles",
                column: "ProgramsId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_SetId",
                table: "Profiles",
                column: "SetId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_WorkoutId",
                table: "Profiles",
                column: "WorkoutId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Addresses_AddressId",
                table: "Profiles",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Goals_GoalId",
                table: "Profiles",
                column: "GoalId",
                principalTable: "Goals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Programs_ProgramsId",
                table: "Profiles",
                column: "ProgramsId",
                principalTable: "Programs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Sets_SetId",
                table: "Profiles",
                column: "SetId",
                principalTable: "Sets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Workouts_WorkoutId",
                table: "Profiles",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Addresses_AddressId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Goals_GoalId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Programs_ProgramsId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Sets_SetId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Workouts_WorkoutId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_AddressId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_GoalId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_ProgramsId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_SetId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_WorkoutId",
                table: "Profiles");
        }
    }
}
