﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeFit_API.Models
{
    public class ProgramWorkout
    {
        public int Id { set; get; }
        [ForeignKey("Program")]
        public int ProgramId { set; get; }
        [ForeignKey("Workout")]
        public int WorkoutId { set; get; }
        public Programs Program { set; get; }
        public Workout Workout { set; get; }
    }
}
