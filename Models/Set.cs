﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeFit_API.Models
{
    public class Set
    {
        public int Id { get; set; }
        public int ExerciseRepetitions { set; get; }
        [ForeignKey("Exercise")]
        public int ExerciseId { set; get; }
        public virtual Profile Profile { get; set; }
        public virtual Workout Workout { get; set; }
    }
}
