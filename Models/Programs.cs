﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeFit_API.Models
{
    public class Programs
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Category { set; get; }

        public virtual Profile Profile { get; set; }
    }
}
