﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeFit_API.Models
{
    public class Goal
    {
        public int Id { set; get; }
        public DateTime EndDate {set; get;}
        public bool Achieved { set; get; }
        [ForeignKey("Programs")]
        public int ProgramsId { set; get; }
        public virtual Profile Profile { get; set; }
    }
}
