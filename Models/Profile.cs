﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeFit_API.Models
{
    public class Profile
    {
        public int Id { set; get; }

        [ForeignKey("User")]
        public int UserId { set; get; }

        [ForeignKey("Goal")]
        public int GoalId { set; get; }

        [ForeignKey("Address")]
        public int AddressId { set; get; }

        [ForeignKey("Programs")]
        public int ProgramsId { set; get; }

        [ForeignKey("Workout")]
        public int WorkoutId { set; get; }

        [ForeignKey("Set")]
        public int SetId { set; get; }

        public float Weight { set; get; }
        public float Height { set; get; }
        [NotMapped]
        public List<string> MedicalConditions { set; get; }
        [NotMapped]
        public List<string> Disabilities { set; get; }




        public Profile()
        {

        }


    }
}
