﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeFit_API.Models
{
    public class Workout
    {
        public int Id { get; set; }
        public string Name { set; get; }
        public string Type { set; get; }
        public bool Complete { set; get; }

        [ForeignKey("Set")]
        public int SetId { set; get; }
        public virtual Set Set { set; get; }
        public virtual Profile Profile { set; get; }
    }
}
