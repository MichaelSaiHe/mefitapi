﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeFit_API.Models
{
    public class GoalWorkout
    {
        public int Id { set; get; }
        [ForeignKey("Workout")]
        public int WorkoutId { set; get; }
        [ForeignKey("Goal")]
        public int GoalId { set; get; }
        public Goal Goal { set; get; }
        public Workout Workout { set; get; }
    }
}
